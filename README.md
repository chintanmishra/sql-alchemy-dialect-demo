# README for using UUID in MySQL from Flask application with Flask-SQLAlchemy, SQLAlchemy, PyMySQL

This project is a demo on how to use UUID with MySQL in an application using Flask-SQLAlchemy. As a result you will notice that this project doesn't use any other Flask feature.

The code shown here was a test done from a StackOverflow answer [here](https://stackoverflow.com/a/812363).

Originally, @ss13ms110 and I were hoping to find a way to use MySQL function `BIN_TO_UUID` when creating our query using SQLAlchemy. Due to unavailability of a straightforward way, we chose to proceed with this.

1. Create a new virtual environment for Python
	```bash
	python3 -m venv venv
	```

2. Install dependencies using
	```bash
	pip install flask-sqlalchemy PyMySQL SQLAlchemy
	```

3. Start Python Interactive Shell
	```bash
	$ . ./venv/bin/activate
	$ python
	```
	Now you have entered Python Interactive REPL
	```shell
	>>> from app import db
	>>> from app import Test
	>>> t = Test()
	>>> db.session.add(t)
	>>> db.session.commit()
	>>> Test.query.filter_by().all()
	[<Test b7e93053-2f48-481f-9979-68d7f2170cd0>, <Test cae0c545-80dd-409f-9606-b0076f073440>]
	>>> p = Test.query.filter_by().all()
	>>> print(p[0].id)
	b7e93053-2f48-481f-9979-68d7f2170cd0
	```

4. Verify if everything behaves as expected
	```SQL
	SELECT
		BIN_TO_UUID(id)
	FROM
		test;
	```